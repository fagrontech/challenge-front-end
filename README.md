# Teste para Pessoa Desenvolvedora de Software Front-End
#### Instruções gerais
- Ao receber o e-mail desse teste, você tem 5 dias para entregar o resultado.
- O resultado deve ser entregue através de um link para um repositório no seu GitHub
pessoal (se você não possui uma conta no GitHub, pode criar uma, é grátis
https://github.com)
- Leia atentamente aos requisitos, são poucos, mas a interpretação também faz parte do
teste. Caso não consiga finalizar tudo em tempo, pode enviar o resultado mesmo assim,
adicionando comentários no código onde teve dificuldades, explicando a intenção e
como havia pensado em resolver o problema. Esse é um teste simples onde as únicas
exigências são: 
A aplicação deve ser Web;
	- Deve-se JS(ES6+) e angularJS como framework para o desenvolvimento front-end. (Será avaliado estrutura de código, boas práticas de desenvolvimento e utilização dos recursos da linguagem e framework);
	- Deve se utilizar CSS para estilização. (Utilização de pré-processadores será considerado um ponto positivo a mais na avaliação);
	- Deve se utilizar Gulp;
	- Deve se utilizar Node como gerenciador de depêndencia;
	- O projeto deve ser responsivo;
	- Será avaliado beleza e qualidade de design aplicado ao projeto;

### Sobre a aplicação a ser desenvolvida: 
O desafio do challenge é desenvolver um Blog com listagem de posts e visualização do post selecionado. 

### Especificação:
#### Login
O Blog deve conter uma página de login, contendo validação de email e senha. Onde caso seja passado usuário e senha diferente do cadastrado ele bloqueie o acesso a pagina interna.
**OBS: Não é necessário criar uma API de autenticação, apenas informar no teste, o usuário e senha corretos para acessar o sistema.**

#### Listagem de Posts
Deve se utilizar as API'S abaixo para listar os posts.
Cada item de listagem de post deve ter a formatação de card, contendo a opção para visualizar o post, editar e excluir.

	URL api base:
	https://jsonplaceholder.typicode.com/

**Como a api é basicamente um mock, os endpoints de POST, PUT e DELETE não irá persistir a alteração ou adição de itens, por isso atente-se em retornar para o usuário se a alteração ocorreu com sucesso ou não.**

- Listagem de posts:
	 Deve se utilizar **/posts(GET)**, para listagem de post

- Criação de posts:
	 Deve se utilizar **/posts(POST)**, para criação de post

- Edição de posts:
	 Deve se utilizar **/posts/{id}(PUT)**, para edição de post

- Exclusão de posts:
	 Deve se utilizar **/posts/{id}(DELETE)**, para exclusão de post


#### Visualização de Post
A visualização de post deve ser composta por uma tela especifica de visualização onde carregue o conteúdo do post e comentários do mesmo.

- Visualizar post:
	 Deve se utilizar **/posts/{id}(GET)** para exibir os detalhes do post;

- Visualizar comentários do post:
	 Deve se utilizar **/comments?postId={id}(GET)** para exibir os detalhes do post;
